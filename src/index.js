import React from 'react';
import ReactDOM from 'react-dom';
import Home from './components/screens/Home';
import Skills from './components/screens/Skills';
import Examples from './components/screens/Examples';
import Contact from './components/screens/Contact';
import './index.css';

ReactDOM.render(
  <div>
  	<Home />
  	<Skills />
  	<Examples />
  	<Contact />
  </div>,
  document.getElementById('root')
);

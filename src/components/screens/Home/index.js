import React, { Component, PropTypes } from 'react';
import Radium from 'radium'
import Button from '../../atoms/Button'
import './index.css';

class Home extends Component {



  render() {
    return (
      <div className="mainContainer">
        <div className="linearGradient"></div>
        <div className="BG">
          <h1>Joe Rosenfield</h1>
          <h3>Front End Web Dev</h3>
          <p>Hit F12 and press CTRL + SHIFT + M</p>
          <p className='hiddenText'>See how responsive and mobile and tablet friendly I am?</p>
          <Button kind='Button1'></Button>
          <Button kind='Button2'></Button>
          <Button kind='Button3'></Button>
        </div>


      </div>
    );
  }
}

export default Radium(Home);

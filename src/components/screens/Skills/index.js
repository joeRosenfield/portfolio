import React, { Component, PropTypes } from 'react';
import Radium from 'radium'
import Button from '../../atoms/Button'
import './index.css';

class Skills extends Component {



  render() {
    return (
      <div className="mainContainer">
        <div className="linearGradient"></div>
        <div className="BG">
          <h1>Skills</h1>
          
          <p>HTML5</p>
          <p>CSS3</p>
          <p>Javascript</p>
          <p>ReactJS</p>
          <p>NPM</p>
          <p>Webpack</p>
          <p>jQuery</p>
          
          <Button kind='Button1'></Button>
          <Button kind='Button2'></Button>
          <Button kind='Button3'></Button>
        </div>


      </div>
    );
  }
}

export default Radium(Skills);

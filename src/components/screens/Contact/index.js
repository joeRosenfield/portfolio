import React, { Component, PropTypes } from 'react';
import Radium from 'radium'
import Button from '../../atoms/Button'
import ContactButton from '../../atoms/ContactButton'
import './index.css';

class Contact extends Component {



  render() {
    return (
      <div className="mainContainer">
        <div className="linearGradient"></div>
        <div className="BG">
          <h1>Contact</h1>
          <ContactButton kind='Facebook'></ContactButton>
          <ContactButton kind='LinkedIn'></ContactButton>
          <ContactButton kind='Gmail'></ContactButton>
          <Button kind='Button1'></Button>
          <Button kind='Button2'></Button>
          <Button kind='Button3'></Button>
        </div>


      </div>
    );
  }
}

export default Radium(Contact);

import React, {PropTypes} from 'react'
import Radium from 'radium'

class Button extends React.Component {

  static propTypes = {
    onClick: PropTypes.func,
    kind: PropTypes.oneOf(['Facebook', 'LinkedIn', 'Gmail']).isRequired
  };

  handleClick() {
      if (this.props.onClick) {
          this.props.onClick();
      }
  }

  render() {

    return (
      <button
        onClick={() => this.handleClick()}
        style={[
          styles.base,
          styles[this.props.kind]]} >
          {this.props.children}
      </button>
    );
  }
}



  var styles = {
    base: {
      position: 'absolute',
      width: '3em',
      height: '3em',
      top: '30%',
      borderStyle: 'none'
    },

    Facebook: {
      background: 'darkblue',
      left: '25%',
      transform: 'translate(-50%, -50%)'
    },

    LinkedIn: {
      background: 'lightblue',
      left: '50%',
      transform: 'translate(-50%, -50%)'
    },

    Gmail: {
      background: 'red',
      right: '25%',
      transform: 'translate(50%, -50%)'

    }
  };



export default Radium(Button);

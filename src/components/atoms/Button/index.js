import React, {PropTypes} from 'react'
import Radium from 'radium'

class Button extends React.Component {

  static propTypes = {
    onClick: PropTypes.func,
    kind: PropTypes.oneOf(['Button1', 'Button2', 'Button3']).isRequired
  };

  handleClick() {
      if (this.props.onClick) {
          this.props.onClick();
      }
  }

  render() {

    return (
      <button
        onClick={() => this.handleClick()}
        style={[
          styles.base,
          styles[this.props.kind]]} >
          {this.props.children}
      </button>
    );
  }
}



  var styles = {
    base: {
      position: 'absolute',
      width: '20%',
      padding: '10% 0',
      margin: '1em auto',
      top: '70%',
      borderRadius: '50%',
      borderStyle: 'none'
    },

    Button1: {
      background: '#ff99db',
      left: '20%',
      transform: 'translate(-50%, -50%)'
    },

    Button2: {
      background: '#6699FF',
      left: '50%',
      transform: 'translate(-50%, -50%)'
    },

    Button3: {
      background: '#9966FF',
      right: '20%',
      transform: 'translate(50%, -50%)'

    }
  };



export default Radium(Button);
